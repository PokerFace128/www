#! /bin/bash
echo
echo "Please run this script under atzlinux-cd dir"
echo "请在 atzlinux-cd 目录下执行该脚本"
echo
echo "cd atzlinux-cd; wget https://www.atzlinux.com/atzlinux-cd/rsync-atzlinux-cd-iso.sh; chmod +x rsync-atzlinux-cd-iso.sh; ./rsync-atzlinux-cd-iso.sh;"

rsync -v -d -r --times --links --hard-links --partial --block-size=8192 --progress rsync://rsync.atzlinux.com:1883/atzlinux-cd .
