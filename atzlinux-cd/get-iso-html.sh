#! /bin/bash
# 生成 iso 下载网页脚本

# iso.list.txt		当前提供 iso 下载文件的路径、文件名信息
# iso-site-cityisp.txt	当前提供 iso 下载的机器域名、端口、城市、网络运营商信息

for pf in `cat iso.list.txt`
do
	echo $pf
# v 版本号
v=`echo $pf|awk -F"/" {'print $3'} `
#echo $v
#a CPU 架构
a=`echo $pf|awk -F"/" {'print $4'} `
#echo $a
#h html 文件名
h="iso-$v$a.htm"
echo $h
# s 站点
# n 城市运营商名字
> $h
	while read s n
	do
	echo "<li>$n</li>" >> $h
	echo "<p>" >> $h
url=`echo $s$pf|sed s%/./%/%`
	echo "<a href=\"$url\" target=\"_blank\">$url</a>" >> $h
	echo "</p>" >> $h
	done < iso-site-cityisp.txt
done
