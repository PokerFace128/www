#! /bin/bash

# iso.list.txt		当前提供 iso 下载文件的路径、文件名信息
# iso-site-cityisp.txt	当前提供 iso 下载的机器域名、端口、城市、网络运营商信息

# 检测脚本运行时发现的错误日志记录文件，该文件只记录当次检测错误情况。
ELOG="check-iso-error.txt"
if [ ! -e $ELOG ];then
	touch $ELOG
fi

# 错误历史记录文件，累计记录每次检测发现的错误
ELOGHISTORY="check-iso-error-history.txt"
if [ ! -e $ELOGHISTORY ];then
	touch $ELOGHISTORY
fi

# timeout 3s
TW="3"

# 在每次检测脚本运行前，清空当次错误日志记录文件
cp /dev/null $ELOG
echo "This check begain at `date +%F"-"%T`, timeout is set to "$TW"s" >> $ELOG

# s 站点
# n 城市运营商名字
# pf path file
while read s n
	do
	for pf in `cat iso.list.txt`
	do
	url=`echo $s$pf|sed s%/./%/%`
	echo $url
	curl -s -m $TW --connect-timeout $TW -I $url
	R=`echo $?`
		if [ $R != 0 ];then
		echo "`date +%Y%m%d%H%M%S` $TW $url" >> $ELOGHISTORY
		echo "`date +%F"-"%T` $TW $url" >> $ELOG
		fi
	done
done < iso-site-cityisp.txt

echo "This check end at `date +%F"-"%T`" >> $ELOG

echo "Total Error Number: "`grep -v "This check" $ELOG|wc -l`
