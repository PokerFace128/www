#!/bin/bash
apt-get -y install wget
wget -c -O atzlinux-archive-keyring_lastest_all.deb https://www.atzlinux.com/atzlinux/pool/main/a/atzlinux-archive-keyring/atzlinux-archive-keyring_lastest_all.deb
apt-get -y install ./atzlinux-archive-keyring_lastest_all.deb
apt-get update
apt-get -y install festival
apt-get -y install stardict-plugin-spell
apt-get -y install sudo
apt-get -y install stardict-dic-data-atzlinux
apt-get -y install stardict-wyabdcrealpeopletts-atzlinux
apt-get -y install stardict-otdrealpeopletts-atzlinux
