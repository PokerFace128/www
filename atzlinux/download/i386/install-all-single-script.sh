#!/bin/bash
echo "开始安装铜豌豆中文套件 ..."
apt -y install wget
apt -y install software-properties-common
wget -c -O atzlinux-archive-keyring_lastest_all.deb https://www.atzlinux.com/atzlinux/pool/main/a/atzlinux-archive-keyring/atzlinux-archive-keyring_lastest_all.deb
apt -y install ./atzlinux-archive-keyring_lastest_all.deb
dpkg --add-architecture i386
apt update
apt -y install xdg-utils
apt -y install xfce4-settings
apt -y install libcanberra-gtk-module
apt -y install \
fcitx-config-common \
fcitx-config-gtk \
fcitx-frontend-all \
fcitx-frontend-qt5 \
fcitx-googlepinyin \
fcitx-m17n \
fcitx-module-x11 \
fcitx-sunpinyin \
fcitx-table-wubi \
fcitx-table-wbpy \
fcitx-ui-classic
apt -y install  sogoupinyin
apt -y install  electronic-wechat
apt -y install  deepin.com.qq.im
apt -y install  youdao-dict
apt -y install  wps-office wps-office-fonts
sed -i '11 i\xfsettingsd --display :0.0 & \n' /opt/deepinwine/apps/Deepin-QQ/run.sh
rm -f /etc/apt/sources.list.d/sogoupinyin.list
apt -y install fonts-zh-cn-misc-atzlinux
echo "安装成功，请退出当前登录，重新登录，让安装生效。"
